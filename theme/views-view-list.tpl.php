<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 */

// If this view is a list of links, then just print that.
if ($output) {
  print $output;
}
else {
  // Otherwise fallback on views template file.
  include drupal_get_path('module', 'views') . '/theme/views-view-list.tpl.php';
}
