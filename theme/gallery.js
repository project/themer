$(function() {
  $('.imgthemer-picture').each(function(galleryIndex) {
    var thumbnailLinks = $('.additional-images a.imagecache', this);
    var largeImg = $('img', this).not($('img', thumbnailLinks));
    thumbnailLinks.each(function(thumbIndex) {
      $(this).click(function() {
        // The full URL to the imagecache directory.
        var imagecacheUrl = Drupal.settings.fileDirectoryUrl + '/imagecache/';

        // Path to NEW un-cached image relative to Drupal's file directory.
        var newImgPath = $('img', this).attr('src').substr(imagecacheUrl.length);
        newImgPath = newImgPath.substr(newImgPath.indexOf('/'));

        // Path to OLD un-cached image relative to Drupal's file directory.
        var oldImgPath = largeImg.attr('src').substr(imagecacheUrl.length);
        oldImgPath = oldImgPath.substr(oldImgPath.indexOf('/'));

        // Update src in largeImg
        largeImg.attr('src', largeImg.attr('src').replace(oldImgPath, newImgPath));
        // We can't know the height and width easily, so just remove them.
        largeImg.removeAttr('height').removeAttr('width');
        // @todo Update alt and title tags too.

        // Update link on largeImg too.
        var imgLink = largeImg.parent('a');
        imgLink.attr('href', imgLink.attr('href').replace(oldImgPath, newImgPath));

        // Prevent event bubbling – don't follow the link.
        return false;
      });
    });
  });
});
