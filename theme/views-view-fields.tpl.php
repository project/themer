<?php

/**
 * @file views-view-field.tpl.php
 * Based on views module's theme/views-view-fields.tpl.php, without the fields.
 * See also viewsthemer-view-field.tpl.php
 * Default simple view template to a field in a row.
 *  - $image:  The imagefield of the row.
 *  - $title:  The title field of the row.
 *  - $fields: The other non-specially themed fields (themed with viewsthemer_view_field).
 */
?>

<?php if ($image): ?><div class="viewsthemer-image"><?php print $image ?></div><?php endif; ?>
<?php if ($title): ?><h3 class="title"><?php print $title ?></h3><?php endif; ?>
<?php print $other_fields ?>
