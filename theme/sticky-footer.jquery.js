/**
 * @file
 *  Fixes the footer to the bottom of the page or the viewport, whichever is lower.
 */

// Resizes page and attaches footer to base.
var stickFooter = function() {
  var marginsHeight = parseInt($('body').css('margin-top')) + parseInt($('body').css('margin-bottom'));
  var windowHeight = $(window).height();
  var page = $(Drupal.settings.selectors.page);
  if (page.height() <= windowHeight) {
    page.height((windowHeight - marginsHeight) + 'px');
    $(Drupal.settings.selectors.footer).addClass('stuck');
  }
};

// Run on document.ready
$(stickFooter);

// Window resize event handler.
$(window).resize(function() {
  // Restore defaults.
  $(Drupal.settings.selectors.footer).removeClass('stuck');
  $(Drupal.settings.selectors.page).height('auto');
  // Stick the footer again.
  stickFooter();
});
