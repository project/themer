<?php

/**
 * @file theme.inc
 * An alias for views module's theme/theme.inc because hook_theme_registry_alter
 * doesn't let you set one path for the include and another path for the template.
 */

require_once drupal_get_path('module', 'views') . '/theme/theme.inc';
