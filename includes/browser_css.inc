<?php
/**
 * @file browser_css.inc
 *  Returns browser-specific CSS for any browser
 */

/**
 * Menu callback for themer/browser.css
 * Detects the browser and version,
 * aggregates and compresses appropriate CSS files,
 * and redirects the browser to the compressed file.
 * This must be a separate HTTP request to the page rendering request in order to be able to cache the page without breaking rendering of other browsers.
 * Call it from hook_init() instead of hook_menu() to improve performance.
 * This is in a separate file so that the code only gets loaded when it is needed.
 * @todo Not tested.
 * @todo Not ported to Drupal 6.
 * @todo Abstract settings for any theme to implement.
 * @see also http://drupal.org/project/conditional_styles
 */
function themer_browser_css() {
  // Reusable paths.
  $zen = drupal_get_path('theme', 'zen');
  $raretheme = drupal_get_path('theme', 'raretheme');

  // Build a drupal_build_css_cache() compatible array of files by type
  // $types is also used to create uniqueness of the filename.
  $types = array();

  /**
   * We need to give special browsers special stylesheets.  Here are some example $_SERVER['HTTP_USER_AGENT'] for different borwsers
   *
   * Safari: Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en) AppleWebKit/419 (KHTML, like Gecko) Safari/419.3
   * Opera: Opera/9.02 (Macintosh; Intel Mac OS X; U; en)
   *      Opera/9.10 (Windows NT 5.1; U; en)
   * FF 2: Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3
   *      Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3
   * FF 1.5: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.11) Gecko/20070312 Firefox/1.5.0.11
   * FF 1.0: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.13) Gecko/20060410 Firefox/1.0.8
   * Camino: Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.0.4) Gecko/20060613 Camino/1.0.2
   * IE7: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)
   */

  /**
   * Use this to detect firefox versions older than FF 1.5.
  if (preg_match('/Gecko\/([0-9]+)/', $_SERVER['HTTP_USER_AGENT'], $matches)) {
    $agent['gecko'] = true;
    // 20060410 is approximately the version of gecko in FF before FF 1.5.
    if ($matches[1] <= 20060410) {
      $agent['gecko_old'] = true;
    }
  }
  */

  // Check for IE and it's version.
  if (preg_match('/MSIE ([0-9]\.[0-9])/', $_SERVER['HTTP_USER_AGENT'], $matches)) {
    $types['ie'] = array();
    $types['ie']["$zen/ie.css"] = true;
    $types['ie']["$raretheme/ie.css"] = true;
    if ($matches[1] < 7) {
      $types['ie6'] = array();
      $types['ie6']["$raretheme/ie6.css"] = true;
    }
  }
  else {
    $types['not-ie'] = array();
    $types['not-ie']["$raretheme/not-ie.css"] = true;
  }

  if (variable_get('preprocess_css', false)) {
    // Create a unique filename for this set of files.
    $filepath = drupal_build_css_cache($types, md5(serialize($types)) .'.css');

    /**
     * Aggregate and compress the files.
     * drupal_build_css_cache() manages caching by only doing anything if the file does not already exist.
     * Database caching would be ideal here, but file-caching will do.
     */
    drupal_goto($GLOBALS['base_url'] . $GLOBALS['base_path'] . $filepath);
  }
  else {
    $css = array();
    $base_path = base_path();
    foreach ($types as $type) {
      foreach ($type as $file => $preprocess) {
        $css[] = "@import \"$base_path$file\";\n";
      }
    }
    drupal_set_header('Content-Type: text/css');
    print implode($css);
    exit;
  }
}

